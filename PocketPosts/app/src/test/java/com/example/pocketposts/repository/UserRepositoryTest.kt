package com.example.pocketposts.repository

import com.example.pocketposts.model.UserDetails
import com.example.pocketposts.model.UserPosts
import com.example.pocketposts.network.IUsersApi
import com.example.pocketposts.network.Resource
import com.example.pocketposts.network.ResponseHandler
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class UserRepositoryTest {
    private val responseHandler = ResponseHandler()
    private lateinit var iUserApi: IUsersApi
    private lateinit var repository: UsersRepository
    private val validUserId = 1
    private val userPost = UserPosts(1, 2, "Title", "Post")
    private val userPostsList: MutableList<UserPosts> = ArrayList()
    private val userPostsResponse = Resource.success(userPostsList)
    private val userDetails = UserDetails(
        1,
        "John Lunde",
        "john",
        "john@gmail.cpm",
        null,
        "+414144444444",
        "jo.com",
        null
    )
    private val userDetailsResponse = Resource.success(userDetails)

    @Before
    fun setUp() {
        iUserApi = mock()
        userPostsList.add(userPost)
        runBlocking {
            whenever(iUserApi.getUsers()).thenReturn(userPostsList)
            whenever(iUserApi.getUserDetails(eq(validUserId))).thenReturn(userDetails)
        }
        repository = UsersRepository(
            iUserApi,
            responseHandler
        )
    }

    @Test
    fun `test get user posts from user repository`() =
        runBlocking {
            assertEquals(userPostsResponse, repository.getPosts())
        }

    @Test
    fun `test get user details from user repository for valid user`() =
        runBlocking {
            assertEquals(userDetailsResponse, repository.getUserDetails(validUserId))
        }
}