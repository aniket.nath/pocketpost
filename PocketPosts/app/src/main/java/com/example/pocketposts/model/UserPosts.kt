package com.example.pocketposts.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserPosts(@SerializedName("userId") val userId : Int,
                     @SerializedName("id") val id : Int,
                     @SerializedName("title") val title : String?,
                     @SerializedName("body") val body : String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(userId)
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(body)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserPosts> {
        override fun createFromParcel(parcel: Parcel): UserPosts {
            return UserPosts(parcel)
        }

        override fun newArray(size: Int): Array<UserPosts?> {
            return arrayOfNulls(size)
        }
    }
}