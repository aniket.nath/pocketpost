package com.example.pocketposts.listeners

interface OnListItemClick {
    fun onListItemClick(userId: Int?)
}