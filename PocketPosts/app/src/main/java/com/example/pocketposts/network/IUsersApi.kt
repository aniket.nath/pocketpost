package com.example.pocketposts.network

import com.example.pocketposts.model.UserDetails
import com.example.pocketposts.model.UserPosts
import retrofit2.http.GET
import retrofit2.http.Path

interface IUsersApi {
    @GET("posts")
    suspend fun getUsers(): List<UserPosts>

    @GET("users/{userId}")
    suspend fun getUserDetails(@Path("userId") userId: Int?): UserDetails
}