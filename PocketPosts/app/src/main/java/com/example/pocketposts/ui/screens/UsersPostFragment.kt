package com.example.pocketposts.ui.screens

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketposts.R
import com.example.pocketposts.databinding.UsersPostFragmentBinding
import com.example.pocketposts.listeners.OnListItemClick
import com.example.pocketposts.model.UserPosts
import com.example.pocketposts.model.UserPostsNo
import com.example.pocketposts.network.Status
import com.example.pocketposts.ui.MainActivity
import com.example.pocketposts.ui.adapter.UsersPostAdapter
import com.example.pocketposts.ui.viewmodels.UsersPostViewModel
import com.example.pocketposts.utils.UserPostsCache
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class UsersPostFragment() : Fragment(),
    OnListItemClick, TextWatcher {

    private val usersPostViewModel: UsersPostViewModel by viewModel()
    private lateinit var binding: UsersPostFragmentBinding
    private var usersPostMap: Map<Int, List<UserPosts>> = HashMap()
    private var usersPostsNoList: MutableList<UserPostsNo> = ArrayList()
    private val adapter: UsersPostAdapter by inject { parametersOf(this@UsersPostFragment) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = UsersPostFragmentBinding.inflate(
            inflater,
            container,
            false)
        setupActionBar()
        initRecyclerView()
        setupData()
        addSearchObserver()
        binding.searchET.editText?.addTextChangedListener(this)
        return binding.root
    }

    private fun initRecyclerView() {
        binding.apply {
            postsRV.layoutManager =
                LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            postsRV.adapter = adapter
        }
    }

    private fun setupActionBar() {
        val actionBar = (activity as MainActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(false)
        actionBar?.setHomeButtonEnabled(false)
        actionBar?.title = getString(R.string.posts)
    }

    private fun setupData() {
        usersPostViewModel.users.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    usersPostMap = it.data?.usersPostMap!!
                    usersPostsNoList = it.data.usersPostsNoList
                    showResult()
                }
                Status.ERROR -> showError(it.message)
                Status.LOADING -> showLoading()
            }
        })
    }

    private fun showError(message: String?) {
        binding.apply {
            postsRV.visibility = View.GONE
            textView.text = message
            textView.visibility = View.VISIBLE
        }
    }

    private fun showResult() {
        binding.apply {
            postsRV.visibility = View.VISIBLE
            textView.visibility = View.GONE
        }
        adapter.addItems(usersPostsNoList)
    }

    private fun showLoading() {
        binding.textView.text = getString(R.string.loading)
    }

    private fun addSearchObserver() {
        usersPostViewModel.search.observe(viewLifecycleOwner, {
            when(it.status){
                Status.SUCCESS -> {
                    usersPostsNoList = it.data as MutableList<UserPostsNo>
                    showResult()
                }
                else -> showError(it.message)
            }
        })
    }

    override fun onListItemClick(userId: Int?) {
        val bundle = bundleOf("userId" to userId)
        UserPostsCache.setUserPost(userId, usersPostMap[userId])
        binding.root.findNavController().navigate(R.id.next_action, bundle)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        usersPostViewModel.updateSearch(s.toString())
    }

    override fun afterTextChanged(s: Editable?) {
    }
}