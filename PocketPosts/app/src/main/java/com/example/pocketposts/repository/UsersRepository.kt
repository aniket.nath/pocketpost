package com.example.pocketposts.repository

import com.example.pocketposts.model.UserDetails
import com.example.pocketposts.model.UserPosts
import com.example.pocketposts.network.IUsersApi
import com.example.pocketposts.network.Resource
import com.example.pocketposts.network.ResponseHandler

class UsersRepository(private val iUsersApi: IUsersApi,
                      private val responseHandler: ResponseHandler) {

    suspend fun getPosts(): Resource<List<UserPosts>> {
        return try {
            val response = iUsersApi.getUsers()
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    suspend fun getUserDetails(userId: Int?): Resource<UserDetails> {
        return try {
            val response = iUsersApi.getUserDetails(userId)
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}