package com.example.pocketposts.ui.viewmodels

import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.example.pocketposts.network.Resource
import com.example.pocketposts.repository.UsersRepository
import kotlinx.coroutines.Dispatchers

class UserDetailsViewModel(private val usersRepository: UsersRepository) : ViewModel(), Observable {

    private val userId = MutableLiveData<Int>()

    fun setUserId(userId: Int?) {
        this.userId.value = userId
    }

    val user = userId.switchMap {
        liveData(Dispatchers.IO) {
            emit(Resource.loading(null))
            emit(usersRepository.getUserDetails(it))
        }
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}