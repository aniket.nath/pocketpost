package com.example.pocketposts.utils

import com.example.pocketposts.model.UserPostsNo
import com.example.pocketposts.network.Resource

class SearchUtil {
    fun getSearchResult(
        userId: String?,
        userPostsNoList: List<UserPostsNo>?
    ): Resource<List<UserPostsNo>> {
        if (userId == null || userId.trim() == "") {
            return Resource.success(userPostsNoList)
        }
        val id = userId.toInt()
        val data: MutableList<UserPostsNo> = ArrayList()
        if (userPostsNoList != null) {
            for (userPost in userPostsNoList) {
                if (id == userPost.userId) {
                    data.add(userPost)
                }
            }
        }
        if (data.isEmpty()) {
            return Resource.error("No data found", null)
        }
        return Resource.success(data)
    }
}