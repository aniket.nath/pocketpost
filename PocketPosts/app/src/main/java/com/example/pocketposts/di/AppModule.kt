package com.example.pocketposts.di

import com.example.pocketposts.listeners.OnListItemClick
import com.example.pocketposts.network.AuthInterceptor
import com.example.pocketposts.network.ResponseHandler
import com.example.pocketposts.network.RetrofitClient.provideLoggingInterceptor
import com.example.pocketposts.network.RetrofitClient.provideOkHttpClient
import com.example.pocketposts.network.RetrofitClient.provideRetrofit
import com.example.pocketposts.network.RetrofitClient.provideUsersApi
import com.example.pocketposts.repository.UsersRepository
import com.example.pocketposts.ui.adapter.UserPostsAdapter
import com.example.pocketposts.ui.adapter.UsersPostAdapter
import com.example.pocketposts.ui.screens.UsersPostFragment
import com.example.pocketposts.ui.viewmodels.UserDetailsViewModel
import com.example.pocketposts.ui.viewmodels.UsersPostViewModel
import com.example.pocketposts.utils.SearchUtil
import org.koin.dsl.module

val networkModule = module {
    factory { AuthInterceptor() }
    factory { provideOkHttpClient(get(), get()) }
    factory { provideLoggingInterceptor() }
    single { provideRetrofit(get()) }
    factory { provideUsersApi(get()) }
    factory { ResponseHandler() }
}

val appModule = module {
    factory { SearchUtil() }
}

val viewModelModule = module {
    factory { UsersPostViewModel(get(), get()) }
    factory { UserDetailsViewModel(get()) }
}

val adapterModule = module {
    factory { (listener: OnListItemClick) -> UsersPostAdapter(listener) }
    factory { UserPostsAdapter() }
}

val usersRepositoryModule = module {
    factory { UsersRepository(get(), get()) }
}