package com.example.pocketposts.utils

import com.example.pocketposts.model.UserPosts
import com.example.pocketposts.model.UserPostsNo
import com.example.pocketposts.model.UsersListModel
import com.example.pocketposts.network.Resource
import com.example.pocketposts.network.Status

object CalculationUtil {
    private fun getUsersPostMap(usersPostList: List<UserPosts>?): Map<Int, List<UserPosts>> {
        val usersPostMap: MutableMap<Int, MutableList<UserPosts>> = HashMap()
        if (usersPostList != null) {
            for (user in usersPostList) {
                if (usersPostMap.containsKey(user.userId)) {
                    usersPostMap[user.userId]?.add(user)
                } else {
                    val userPostsList: MutableList<UserPosts> = ArrayList()
                    userPostsList.add(user)
                    usersPostMap[user.userId] = userPostsList
                }
            }
        }
        return usersPostMap
    }

    private fun getUsersPostsNoList(usersPostMap: Map<Int, List<UserPosts>>): MutableList<UserPostsNo> {
        val usersPostNoList: MutableList<UserPostsNo> = ArrayList()
        for (userId in usersPostMap.keys) {
            val userPostsNo = UserPostsNo()
            userPostsNo.userId = userId
            userPostsNo.noOfPosts = usersPostMap[userId]?.size
            usersPostNoList.add(userPostsNo)
        }
        return usersPostNoList
    }

    fun getUserListModel(response: Resource<List<UserPosts>>): Resource<UsersListModel> {
        return when (response.status) {
            Status.SUCCESS -> {
                val usersPostMap = getUsersPostMap(response.data)
                Resource.success(
                    UsersListModel(
                        usersPostMap,
                        getUsersPostsNoList(usersPostMap)
                    )
                )
            }
            else -> Resource.error(response.message!!, null)
        }
    }
}