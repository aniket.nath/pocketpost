package com.example.pocketposts.utils

import com.example.pocketposts.model.UserPosts
import java.util.*
import kotlin.collections.HashMap

object UserPostsCache {
    private val usersPostMap: MutableMap<Int?, List<UserPosts>?> = HashMap()

    fun setUserPost(userId: Int?, userPostsList: List<UserPosts>?) {
        usersPostMap[userId] = userPostsList
    }

    fun getUserPostsList(userId: Int?): List<UserPosts>? {
        return usersPostMap[userId]
    }
}