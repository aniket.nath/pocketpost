package com.example.pocketposts.ui.viewmodels

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.example.pocketposts.model.UserPostsNo
import com.example.pocketposts.network.Resource
import com.example.pocketposts.repository.UsersRepository
import com.example.pocketposts.utils.CalculationUtil
import com.example.pocketposts.utils.SearchUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UsersPostViewModel(
    private val usersRepository: UsersRepository,
    private val searchUtil: SearchUtil
) : ViewModel(), Observable {
    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    private val searchUserId = MutableLiveData<String>()

    private val usersPostsNoList = MutableLiveData<List<UserPostsNo>>()

    val users = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        val usersListModel = CalculationUtil.getUserListModel(usersRepository.getPosts())
        emit(usersListModel)
        withContext(Dispatchers.Main) {
            usersPostsNoList.value = usersListModel.data?.usersPostsNoList
        }
    }

    val search = searchUserId.switchMap {
        liveData(Dispatchers.IO) {
            emit(searchUtil.getSearchResult(searchUserId.value, usersPostsNoList.value))
        }
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
        callbacks.remove(callback)
    }

    fun updateSearch(search: String) {
        searchUserId.value = search
    }
}