package com.example.pocketposts.model

class UserPostsNo() {
    constructor(userId: Int, noOfPosts: Int) : this() {
        this.userId = userId
        this.noOfPosts = noOfPosts
    }

    var userId: Int? = null
    var noOfPosts: Int? = null
}