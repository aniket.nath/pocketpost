package com.example.pocketposts.ui.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketposts.R
import com.example.pocketposts.databinding.UserDetailsFragmentBinding
import com.example.pocketposts.model.UserDetails
import com.example.pocketposts.model.UserPosts
import com.example.pocketposts.network.Status
import com.example.pocketposts.ui.MainActivity
import com.example.pocketposts.ui.adapter.UserPostsAdapter
import com.example.pocketposts.ui.viewmodels.UserDetailsViewModel
import com.example.pocketposts.utils.UserPostsCache
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class UserDetailsFragment : Fragment() {
    private val viewModel: UserDetailsViewModel by viewModel()
    private lateinit var binding: UserDetailsFragmentBinding
    private var userId: Int? = null
    private var userPostsList: List<UserPosts>? = ArrayList()
    private val adapter: UserPostsAdapter by inject()
    private var actionBar: ActionBar?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.user_details_fragment,
            container,
            false
        )
        setupActionBar()
        initArguments()
        initRecyclerView()
        setUserDetailsDataObserver()
        viewModel.setUserId(userId)
        return binding.root
    }

    private fun initArguments() {
        userId = arguments?.getInt("userId")
        userPostsList = UserPostsCache.getUserPostsList(userId)
    }

    private fun setupActionBar() {
        actionBar = (activity as MainActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        actionBar?.title = ""
    }

    private fun initRecyclerView() {
        binding.apply {
            userPostsRV.layoutManager =
                LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            userPostsRV.adapter = adapter
            userPostsRV.isNestedScrollingEnabled = false
            adapter.addItems(userPostsList)
        }
    }

    private fun setUserDetailsDataObserver() {
        viewModel.user.observe(viewLifecycleOwner, {
            when(it.status){
                Status.SUCCESS -> setupScreen(it.data)
                Status.ERROR -> showError(it.message)
                Status.LOADING -> showLoading()
            }
        })
    }

    private fun showError(message: String?) {
        binding.loading.text = message
    }

    private fun showLoading() {
        binding.loading.text = getString(R.string.loading)
    }

    private fun setupScreen(userDetails: UserDetails?) {
        actionBar?.title = userDetails?.name
        binding.apply {
            loading.visibility = View.GONE
            heading.visibility = View.VISIBLE
            nameTitle.visibility = View.VISIBLE
            emailTitle.visibility = View.VISIBLE
            user = userDetails
        }
    }
}