package com.example.pocketposts.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}