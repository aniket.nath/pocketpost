package com.example.pocketposts.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.pocketposts.R
import com.example.pocketposts.databinding.RowUserPostsBinding
import com.example.pocketposts.listeners.OnListItemClick
import com.example.pocketposts.model.UserPostsNo

class UsersPostAdapter(private val listener: OnListItemClick) :
    RecyclerView.Adapter<UsersPostAdapter.ViewHolder>() {
    private var items: List<UserPostsNo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsBinding: RowUserPostsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_user_posts, parent, false
        )
        return ViewHolder(itemsBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            holder.itemBinding.user = items[position]
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(items: List<UserPostsNo>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(val itemBinding: RowUserPostsBinding) :
        RecyclerView.ViewHolder(itemBinding.root),
        View.OnClickListener {
        init {
            itemBinding.root.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            listener.onListItemClick(items[adapterPosition].userId)
        }

    }
}