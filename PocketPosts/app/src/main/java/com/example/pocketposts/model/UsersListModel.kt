package com.example.pocketposts.model

data class UsersListModel(
    val usersPostMap: Map<Int, List<UserPosts>>,
    val usersPostsNoList: MutableList<UserPostsNo>
)