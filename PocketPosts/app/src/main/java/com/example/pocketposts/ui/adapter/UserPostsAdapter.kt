package com.example.pocketposts.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.pocketposts.R
import com.example.pocketposts.databinding.RowPostsBinding
import com.example.pocketposts.model.UserPosts

class UserPostsAdapter : RecyclerView.Adapter<UserPostsAdapter.ViewHolder>() {
    private var items: MutableList<UserPosts> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsBinding: RowPostsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_posts, parent, false
        )
        return ViewHolder(itemsBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items.isNotEmpty()) {
            holder.itemBinding.user = items[position]
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(items: List<UserPosts>?) {
        if (items != null) {
            this.items.clear()
            this.items.addAll(items)
        }
    }

    inner class ViewHolder(val itemBinding: RowPostsBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }
}